import React, { Component } from "react";
import { connect } from "react-redux";
import { changeNumber } from "../redux/actions/actionDemoRedux";

class DemoRedux extends Component {
  render() {
    return (
      <div className="container py-5">
        <button
          onClick={() => this.props.increaseNumber(-1)}
          className="btn btn-success"
        >
          -
        </button>
        <span className="mx-3">{this.props.number}</span>
        <button
          onClick={() => this.props.increaseNumber(1)}
          className="btn btn-success"
        >
          +
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    number: state.numberReducer.number,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    increaseNumber: (value) => {
      dispatch(changeNumber(value));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DemoRedux);
