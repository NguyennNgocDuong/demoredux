import { CHANGENUMBER } from "../constants/constantsDemoRedux"

const initialState = {
    number: 1
}

export let numberReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case CHANGENUMBER: {
            state.number += payload
            return { ...state }
        }
        default:
            return state
    }
}
