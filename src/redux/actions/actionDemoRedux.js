import { CHANGENUMBER } from "../constants/constantsDemoRedux";

export let changeNumber = (value) => {
    return {
        type: CHANGENUMBER,
        payload: value,
    }


}