import logo from './logo.svg';
import './App.css';
import DemoRedux from './components/DemoRedux';

function App() {
  return (
    <div className="App">
      <DemoRedux />
    </div>
  );
}

export default App;
